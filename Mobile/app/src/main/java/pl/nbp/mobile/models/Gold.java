package pl.nbp.mobile.models;

import com.google.gson.annotations.SerializedName;

public class Gold {
    @SerializedName("data")
    private String data;
    @SerializedName("cena")
    private float price;


    public String getData() {
        return data;
    }

    public float getPrice() {
        return price;
    }
}
