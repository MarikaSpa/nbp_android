package pl.nbp.mobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
// główna metoda uruchamiajaca onCreate

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // ustawienie disainu z folderu layout
        setContentView(R.layout.activity_main);

        setupUi();
    }


    private void setupUi() {
        Button first = findViewById(R.id.first_button);
        Button second = findViewById(R.id.second_button);

        first.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //nowy obiekt intent w którym podajemy klase głowną i klase do której ma się odnosić kliekniecie
                Intent intent = new Intent(MainActivity.this,
                        GoldActivity.class);
                startActivity(intent);
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CurrencyActivity.class);

                startActivity(intent);
            }
        });

    }

}
