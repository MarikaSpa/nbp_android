package pl.nbp.mobile;

import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import pl.nbp.mobile.models.Rate;
import pl.nbp.mobile.models.Table;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyActivity extends AppCompatActivity {

    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);
// metoda do pobrania danych
        fetchData();
        //metoda do ustawienie paska
        setToolbar();
        progressBar = findViewById(R.id.progress_bar);
    }


    private void setToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Kursy walut");

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //obsługa strzałki gurnej do cofania
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchData() {
        Call<List<Table>> call = NbpApi.getApi().getCurrencies();
        call.enqueue(getCallback());


    }

    private Callback<List<Table>> getCallback() {

        return new Callback<List<Table>>() {
            @Override
            public void onResponse(Call<List<Table>> call, Response<List<Table>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null && !response.body().isEmpty()) {

                    showData(response.body().get(0));

                }

            }

            @Override
            public void onFailure(Call<List<Table>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                // wyswietlenie informacji co spowodowało błąd
                t.printStackTrace();
                String message = t.getMessage();

                TextView error = findViewById(R.id.error);
                error.setText(message);
                error.setVisibility(View.VISIBLE);


            }
        };
    }

    private void showData(Table table) {
        RecyclerView recyclerView = findViewById(R.id.recycle_view);
        CurrencyAdapter adapter = new CurrencyAdapter(table.getRates());
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration divider = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(divider);
    }

}
